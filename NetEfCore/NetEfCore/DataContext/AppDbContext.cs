﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace NetEfCore.DataContext
{
    public class AppDbContext : DbContext
    {
        
            // injection
            protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            {
            optionsBuilder.UseMySQL("server=localhost;database=ef_db;uid=root;pwd=12345");
            }

            public DbSet<CategoryEntity> categories { get; set; }
        

    }
}
